terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.55.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.4.3"
    }
  }
  # backend "s3" {
  #   #bucket = var.aws_backend_bucket
  #   key    = "myinfra/prod/terraform.tfstate"
  #   region = "us-west-2"
  # }
}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_access_key
  region     = var.aws_region
}

