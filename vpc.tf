
resource "aws_vpc" "main_vpc" {
  cidr_block           = "192.168.0.0/16"
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags                 = local.common_tags

}


resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.main_vpc.id

  tags = local.common_tags

}
resource "aws_route_table" "default_routing_table" {
  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }



  tags = local.common_tags

}

resource "aws_main_route_table_association" "default_vpc_route_table" {
  vpc_id         = aws_vpc.main_vpc.id
  route_table_id = aws_route_table.default_routing_table.id
}

resource "aws_subnet" "public_subnet" {
  count  = length(data.aws_availability_zones.available.names)
  vpc_id = aws_vpc.main_vpc.id
  #cidr_block = "192.168.${1+count.index}.0/24"
  cidr_block              = "192.168.${count.index}.0/24"
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true
  tags = merge(
    local.common_tags,
    {
      Name = "public_subnet_${count.index}"
    },
  )

}

resource "aws_subnet" "private_subnet" {
  count  = length(data.aws_availability_zones.available.names)
  vpc_id = aws_vpc.main_vpc.id
  #cidr_block = "192.168.${1+count.index}.0/24"
  cidr_block              = "192.168.${10 + count.index}.0/24"
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true
  tags = merge(
    local.common_tags,
    {
      Name = "private_subnet${count.index}"
    },
  )

}