# My terraform infra
 this repo will be used to configure my infrastructure for my own awesome project

### Requirements:

1. The S3 bucket that is to be used as a backend should exist prior to the tf execution.

2. Set your AWS environment variables using env.sh. The same script produces the terraform vars.

### TF steps:
1.  Upload my ssh key pair
2.  Create an s3 bucket for our backups
	1.  Enable bucket versioning
	2.  Block all bucket public access
3.  Create a vpc
4.  Create and assign a default route
5.  Create and assign an internet gateway to the default route table
6.  Create public and private subnets on all AZs
7.  Create an iam policy, role and instance profile to allow ec2 instances to register to ecs cluster
8.  Create an ecs cluster
	10.  Create a launch template. Populate userdata to register to the eco cluster
	11.  Create an autoscaling group
	12.  Create a security group to allow inbound traffic (http from everywhere, everything from office ip). Assign it to the launch template
9.  Create an ecr registry for my-application (unused for now)
10. Create an RDS instance
11. Deploy a wordpress instance and link it to RDS 
12. Use EFS as storage backend for wordpress 
13. Expose the wordpress instance through an ALB