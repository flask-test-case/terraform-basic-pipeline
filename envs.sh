#!/bin/zsh

echo -n "Enter your access_key: " 
read ACCESS_KEY

echo -n "Enter your secret accesskey: " 
read SECRET_ACCESS_KEY

echo -n "Enter your default region: " 
read REGION

echo -n "Enter your backend bucket name: " 
read BACKEND_BUCKET_NAME

echo "export AWS_ACCESS_KEY_ID=$ACCESS_KEY"
echo "export AWS_SECRET_ACCESS_KEY=$SECRET_ACCESS_KEY"
echo "export AWS_REGION=$REGION"


echo "export TF_VAR_aws_access_key=$ACCESS_KEY"
echo "export TF_VAR_aws_secret_access_key=$SECRET_ACCESS_KEY"
echo "export TF_VAR_aws_backend_bucket=$BACKEND_BUCKET_NAME"
