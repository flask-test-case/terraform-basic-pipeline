variable "aws_region" {
  type        = string
  description = "AWS region to be used"
  default     = "us-west-2"
}

variable "aws_access_key" {
  type        = string
  description = "AWS access key"
  sensitive   = true
}

variable "aws_secret_access_key" {
  type        = string
  description = "AWS secret access key"
  sensitive   = true
}

variable "aws_backend_bucket" {
  type        = string
  description = "AWS secret access key"
  sensitive   = true
  default     = "my-statefile-bucket"
}
