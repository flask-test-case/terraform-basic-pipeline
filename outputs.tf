
# output "fmar-test-bucket-website" {
#   value = aws_s3_bucket.fmar-test-bucket.website_endpoint
# }

# output "public_subnets" {
#   value = aws_subnet.public_subnet[*].id

# }

# output "private_subnets" {
#   value = aws_subnet.public_subnet[*].id

# }

# output "alb" {
#   value = aws_lb.alb.dns_name
# }

# output "wordpress_port" {
#   value = aws_ecs_task_definition.wordpress_service.container_definitions
# }

# output "efs_ip_address" {
#   value = aws_efs_mount_target.backup_efs_mount_target.ip_address
# }

output "endpoint" {
  value = aws_eks_cluster.my_eks_cluster.endpoint
}

