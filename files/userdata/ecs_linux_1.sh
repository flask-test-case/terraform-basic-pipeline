#!/bin/bash
echo ECS_CLUSTER=${aws_ecs_cluster.ecs_linux_1.id} >> /etc/ecs/ecs.config

#mount efs
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${aws_efs_mount_target.backup_efs_mount_target.ip_address}:/ /mnt
  