locals {
  common_tags = {
    module  = "my-infra"
    company = "fmar-ike"
  }
  my_key_pair                 = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDAHxEohcdVMaIJCVbjkhBwRJEWc0T42vAZoQwtK99+bvLhForO3Lhm69tteaoZzDKIwoUmktRBxAKuOSe93wjvOecwC1/Bn9h0S8K4jEV7fNUMx4fplGBE0ENEv5cIppuwvDUWdaDWeP22zySQ2ECdb1FtJYWlUTKFQnOsfjMbp+QUaA9hE/7QxGTlWXkQYrhbiVwM6nA6tyZEo3GmYPt14ZUTjjw+y5q4X46YQoT8Rf67yTtb7tGE4tVPm/YvFQh8/fDOwrnjHbjmzN7kGOQJXBxcN6a9r+t5BtWswNRKBsieAuyHZikS4YQPj0Rfx8WMkhogfCUT54kmVkwcvAxBuKXqEOzv6kfHFmznYPHPKQHEpaqtMS6J1WqrNf/0ecp6FrN7/7FEXnaE3WiJFmQvmPVC2LsSHVstZECKo6a2n45puAsBFDgr1jLZ4osFqSEjYwxuquWjTNuT/sBT6TtIgVQ8doCnvyKRPoYVE732uZ3oasn2axhlwB1FecBeGy8="
  ecs_linux_instance_register = <<EOF
#!/bin/bash
echo ECS_CLUSTER=${aws_ecs_cluster.ecs_linux_1.id} >> /etc/ecs/ecs.config

#mount efs
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${aws_efs_mount_target.backup_efs_mount_target.ip_address}:/ /mnt
   
#create wordpress directory if it does not exist
mkdir -p /mnt/wordpress/

   EOF
}

# data "local_file" "ecs_linux_1" {
#   filename = "${path.module}/files/userdata/ecs_linux_1.sh"
# }


data "aws_ami" "amazon_linux_2" {
  most_recent = true

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn-ami-2018.03.20210413-amazon-ecs-optimized"]
    # values = ["amzn2-ami-hvm-*-x86_64-ebs"]
  }
}

data "aws_availability_zones" "available" {}



resource "random_integer" "rand" {
  min = 10000
  max = 99999
}


data "aws_iam_policy_document" "tf_ecsInstanceRole" {
  statement {
    # sid = "tf_ecsInstanceRole2"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "eks_cluster_assume_role" {
  statement {

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }
  }
}


data "aws_iam_policy_document" "eks_node_assume_role" {
  statement {

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}