resource "aws_iam_policy" "tf_ecsInstanceRolePolicy" {
  name = "tf_ecsInstanceRolePolicy"
  #   path        = "/"
  description = "tf_ecsInstanceRolePolicy"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "ec2:DescribeTags",
          "ecs:CreateCluster",
          "ecs:DeregisterContainerInstance",
          "ecs:DiscoverPollEndpoint",
          "ecs:Poll",
          "ecs:RegisterContainerInstance",
          "ecs:StartTelemetrySession",
          "ecs:UpdateContainerInstancesState",
          "ecs:Submit*",
          "ecr:GetAuthorizationToken",
          "ecr:BatchCheckLayerAvailability",
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ],
        "Resource" : "*"
      }
    ]
  })
}



resource "aws_iam_role" "tf_ecsInstanceRole" {
  name               = "tf_ecsInstanceRole"
  assume_role_policy = data.aws_iam_policy_document.tf_ecsInstanceRole.json

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  inline_policy {
    name   = aws_iam_policy.tf_ecsInstanceRolePolicy.name
    policy = aws_iam_policy.tf_ecsInstanceRolePolicy.policy
  }

}


resource "aws_iam_instance_profile" "tf_ecsInstanceRoleProfile" {
  name = "tf_ecsInstanceRoleProfile"
  role = aws_iam_role.tf_ecsInstanceRole.name
}


#### EKS
# resource "aws_iam_policy" "AmazonEKSClusterPolicy" {
#   name = "AmazonEKSClusterPolicy_tf"
#   #   path        = "/"
#   description = "AmazonEKSClusterPolicy_tf"
#   policy = jsonencode({
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "autoscaling:DescribeAutoScalingGroups",
#                 "autoscaling:UpdateAutoScalingGroup",
#                 "ec2:AttachVolume",
#                 "ec2:AuthorizeSecurityGroupIngress",
#                 "ec2:CreateRoute",
#                 "ec2:CreateSecurityGroup",
#                 "ec2:CreateTags",
#                 "ec2:CreateVolume",
#                 "ec2:DeleteRoute",
#                 "ec2:DeleteSecurityGroup",
#                 "ec2:DeleteVolume",
#                 "ec2:DescribeInstances",
#                 "ec2:DescribeRouteTables",
#                 "ec2:DescribeSecurityGroups",
#                 "ec2:DescribeSubnets",
#                 "ec2:DescribeVolumes",
#                 "ec2:DescribeVolumesModifications",
#                 "ec2:DescribeVpcs",
#                 "ec2:DescribeDhcpOptions",
#                 "ec2:DescribeNetworkInterfaces",
#                 "ec2:DescribeAvailabilityZones",
#                 "ec2:DetachVolume",
#                 "ec2:ModifyInstanceAttribute",
#                 "ec2:ModifyVolume",
#                 "ec2:RevokeSecurityGroupIngress",
#                 "ec2:DescribeAccountAttributes",
#                 "ec2:DescribeAddresses",
#                 "ec2:DescribeInternetGateways",
#                 "elasticloadbalancing:AddTags",
#                 "elasticloadbalancing:ApplySecurityGroupsToLoadBalancer",
#                 "elasticloadbalancing:AttachLoadBalancerToSubnets",
#                 "elasticloadbalancing:ConfigureHealthCheck",
#                 "elasticloadbalancing:CreateListener",
#                 "elasticloadbalancing:CreateLoadBalancer",
#                 "elasticloadbalancing:CreateLoadBalancerListeners",
#                 "elasticloadbalancing:CreateLoadBalancerPolicy",
#                 "elasticloadbalancing:CreateTargetGroup",
#                 "elasticloadbalancing:DeleteListener",
#                 "elasticloadbalancing:DeleteLoadBalancer",
#                 "elasticloadbalancing:DeleteLoadBalancerListeners",
#                 "elasticloadbalancing:DeleteTargetGroup",
#                 "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
#                 "elasticloadbalancing:DeregisterTargets",
#                 "elasticloadbalancing:DescribeListeners",
#                 "elasticloadbalancing:DescribeLoadBalancerAttributes",
#                 "elasticloadbalancing:DescribeLoadBalancerPolicies",
#                 "elasticloadbalancing:DescribeLoadBalancers",
#                 "elasticloadbalancing:DescribeTargetGroupAttributes",
#                 "elasticloadbalancing:DescribeTargetGroups",
#                 "elasticloadbalancing:DescribeTargetHealth",
#                 "elasticloadbalancing:DetachLoadBalancerFromSubnets",
#                 "elasticloadbalancing:ModifyListener",
#                 "elasticloadbalancing:ModifyLoadBalancerAttributes",
#                 "elasticloadbalancing:ModifyTargetGroup",
#                 "elasticloadbalancing:ModifyTargetGroupAttributes",
#                 "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
#                 "elasticloadbalancing:RegisterTargets",
#                 "elasticloadbalancing:SetLoadBalancerPoliciesForBackendServer",
#                 "elasticloadbalancing:SetLoadBalancerPoliciesOfListener",
#                 "kms:DescribeKey"
#             ],
#             "Resource": "*"
#         },
#         {
#             "Effect": "Allow",
#             "Action": "iam:CreateServiceLinkedRole",
#             "Resource": "*",
#             "Condition": {
#                 "StringEquals": {
#                     "iam:AWSServiceName": "elasticloadbalancing.amazonaws.com"
#                 }
#             }
#         }
#     ]
# })
# }
# resource "aws_iam_policy" "AmazonEKSWorkerPolicy" {
#   name = "AmazonEKSWorkerPolicy_tf"
#   #   path        = "/"
#   description = "AmazonEKSWorkerPolicy_tf"
#   policy = jsonencode({
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "ec2:DescribeInstances",
#                 "ec2:DescribeInstanceTypes",
#                 "ec2:DescribeRouteTables",
#                 "ec2:DescribeSecurityGroups",
#                 "ec2:DescribeSubnets",
#                 "ec2:DescribeVolumes",
#                 "ec2:DescribeVolumesModifications",
#                 "ec2:DescribeVpcs",
#                 "eks:DescribeCluster"
#             ],
#             "Resource": "*"
#         }
#     ]
# })
# }

resource "aws_iam_role" "eks_cluster_role" {
  name               = "eks-cluster-role"
  assume_role_policy = data.aws_iam_policy_document.eks_cluster_assume_role.json

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  # inline_policy {
  #   name   = "AmazonEKSClusterPolicy"
  #   policy = data.aws_iam_policy.eks_cluster_policy.policy
  # }

}

resource "aws_iam_role_policy_attachment" "eks_cluster_role_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_cluster_role.name
}


resource "aws_iam_role" "eks_node_role" {
  name               = "eks-node-role"
  assume_role_policy = data.aws_iam_policy_document.eks_node_assume_role.json

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  # inline_policy {
  #   name   = "AmazonEKSWorkerPolicy"
  #   policy = aws_iam_policy.AmazonEKSWorkerPolicy.policy
  # }

}


resource "aws_iam_role_policy_attachment" "eks_node_role_policy_eksworker" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks_node_role.name
}
resource "aws_iam_role_policy_attachment" "eks_node_role_policy_ec2toecr" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks_node_role.name
}
resource "aws_iam_role_policy_attachment" "eks_node_role_policy_ec2toecr_ekscni" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks_node_role.name
}