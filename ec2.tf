resource "aws_key_pair" "my_key_pair" {
  key_name   = "my-key"
  public_key = local.my_key_pair
  tags       = local.common_tags
}

resource "aws_placement_group" "placement_group_spread" {
  name         = "placement_group_spread"
  strategy     = "spread"
  spread_level = "rack"
}



resource "aws_launch_template" "launch_temp_ecs_linux_1" {
  name_prefix            = "launch_temp_ecs_linux_1"
  image_id               = data.aws_ami.amazon_linux_2.id
  instance_type          = "t3.micro"
  vpc_security_group_ids = [aws_security_group.sg_ecs_linux_1.id]
  update_default_version = true
  key_name               = aws_key_pair.my_key_pair.key_name
  iam_instance_profile {
    name = aws_iam_instance_profile.tf_ecsInstanceRoleProfile.id
  }
  # user_data = filebase64("${path.module}/files/userdata/ecs_linux_1.sh")
  # user_data = base64encode(local.ecs_linux_instance_register)
  user_data = base64encode(local.ecs_linux_instance_register)

}





resource "aws_autoscaling_group" "asg_ecs_linux_1" {
  name                      = "asg_ecs_linux_1"
  max_size                  = 5
  min_size                  = 0
  health_check_grace_period = 300
  health_check_type         = "EC2"
  desired_capacity          = 1
  force_delete              = true
  placement_group           = aws_placement_group.placement_group_spread.id
  launch_template {
    id      = aws_launch_template.launch_temp_ecs_linux_1.id
    version = "$Latest"
  }
  vpc_zone_identifier = [aws_subnet.public_subnet.0.id, aws_subnet.public_subnet.1.id, aws_subnet.public_subnet.2.id, aws_subnet.public_subnet.3.id]

}


resource "aws_security_group" "sg_ecs_linux_1" {
  name        = "sg_ecs_linux_1"
  description = "Allow HTTP(s)/SSH inbound traffic"
  vpc_id      = aws_vpc.main_vpc.id

  ingress {
    description     = "connections from alb"
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.alb.id]
  }

  # ingress {
  #   description     = "connections from other cluster nodes"
  #   from_port       = 0
  #   to_port         = 0
  #   protocol        = "-1"
  #   security_groups = [aws_security_group.sg_ecs_linux_1.id]
  # }

  ingress {
    description = "full access from office"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["208.127.131.51/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "sg_rds_wordpress" {
  name        = "sg_rds_wordpress"
  description = "Allow internal access"
  vpc_id      = aws_vpc.main_vpc.id

  ingress {
    description     = "3306 from ec2"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.sg_ecs_linux_1.id]
  }

  ingress {
    description = "full access from office"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["208.127.131.51/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}



resource "aws_security_group" "sg_efs" {
  name   = "sg_efs"
  vpc_id = aws_vpc.main_vpc.id

  ingress {
    description     = "nfs access from ec2 asg"
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.sg_ecs_linux_1.id]
  }

  ingress {
    description = "full access from office"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["208.127.131.51/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "alb" {
  name        = "sg_alb"
  description = "Allow HTTP(s)/SSH inbound traffic"
  vpc_id      = aws_vpc.main_vpc.id

  ingress {
    description = "TLS from web"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "http from web"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}






resource "aws_lb" "alb" {
  name               = "alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb.id]
  # subnets            = [aws_subnet.public_subnet[0].id, aws_subnet.public_subnet[1].id]
  subnets                    = aws_subnet.public_subnet.*.id
  enable_deletion_protection = false

  tags = local.common_tags
}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_target_group.arn
  }
}


resource "aws_lb_target_group" "alb_target_group" {
  name        = "albtargetgroup"
  target_type = "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main_vpc.id
  health_check {
    path = "/"
    # port                = 8080
    protocol            = "HTTP"
    healthy_threshold   = 2
    unhealthy_threshold = 2
    matcher             = "200-499"
  }
}