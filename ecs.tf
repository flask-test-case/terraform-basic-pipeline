resource "aws_ecs_cluster" "ecs_linux_1" {
  name = "ecs_linux_1"
}

resource "aws_ecs_task_definition" "wordpress_task_def" {
  family = "wordpress"
  container_definitions = jsonencode([
    {
      name      = "wordpress"
      image     = "wordpress"
      memory    = 350
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 0
        }
      ]
      "environment" : [
        { "name" : "WORDPRESS_DB_HOST", "value" : "${aws_db_instance.default.address}" },
        { "name" : "WORDPRESS_DB_USER", "value" : "admin" },
        { "name" : "WORDPRESS_DB_PASSWORD", "value" : "5muQx5C3jzRLys5vEm5Mn" },
        { "name" : "WORDPRESS_DB_NAME", "value" : "wordpress" }
      ],
      mountPoints = [{
        sourceVolume  = "wordpress-volume"
        containerPath = "/var/www/html"
        readOnly      = false
      }]
    }
  ])

  volume {
    name      = "wordpress-volume"
    host_path = "/mnt/wordpress/"
  }

  #   placement_constraints {
  #     type       = "memberOf"
  #     expression = "attribute:ecs.availability-zone in [us-west-2a, us-west-2b]"
  #   }
}


resource "aws_ecs_service" "ecs-linux-1-cluster-service" {
  name                               = "wordpress"
  cluster                            = aws_ecs_cluster.ecs_linux_1.id
  task_definition                    = aws_ecs_task_definition.wordpress_task_def.arn
  desired_count                      = 1
  deployment_minimum_healthy_percent = 0

  #   iam_role        = aws_iam_role.foo.arn
  #   depends_on      = [aws_iam_role_policy.foo]

  #   ordered_placement_strategy {
  #     type  = "binpack"
  #     field = "cpu"
  #   }

  load_balancer {
    target_group_arn = aws_lb_target_group.alb_target_group.arn
    container_name   = "wordpress"
    container_port   = 80
  }

  #   placement_constraints {
  #     type       = "memberOf"
  #     expression = "attribute:ecs.availability-zone in [us-west-2a, us-west-2b]"
  #   }
}





############    testing
resource "aws_ecs_task_definition" "phpmyadmin_task_def" {
  family = "phpmyadmin"
  container_definitions = jsonencode([
    {
      name      = "phpmyadmin"
      image     = "phpmyadmin"
      memory    = 150
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 0
        }
      ]
      "environment" : [
        { "name" : "PMA_HOST", "value" : "${aws_db_instance.default.address}" },
        { "name" : "WORDPRESS_DB_USER", "value" : "admin" }

      ],
    }
  ])

  #   volume {
  #     name      = "service-storage"
  #     host_path = "/ecs/service-storage"
  #   }

  #   placement_constraints {
  #     type       = "memberOf"
  #     expression = "attribute:ecs.availability-zone in [us-west-2a, us-west-2b]"
  #   }
}


resource "aws_ecs_service" "phpmyadmin_service" {
  name                               = "phpmyadmin"
  cluster                            = aws_ecs_cluster.ecs_linux_1.id
  task_definition                    = aws_ecs_task_definition.phpmyadmin_task_def.arn
  desired_count                      = 1
  deployment_minimum_healthy_percent = 0
  #   iam_role        = aws_iam_role.foo.arn
  #   depends_on      = [aws_iam_role_policy.foo]

  #   ordered_placement_strategy {
  #     type  = "binpack"
  #     field = "cpu"
  #   }

  # load_balancer {
  #   target_group_arn = aws_lb_target_group.alb_target_group.arn
  #   container_name   = "phpmyadmin"
  #   container_port   = 80
  # }

  #   placement_constraints {
  #     type       = "memberOf"
  #     expression = "attribute:ecs.availability-zone in [us-west-2a, us-west-2b]"
  #   }
}