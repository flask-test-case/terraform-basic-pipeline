resource "aws_efs_file_system" "backup_efs" {
  creation_token = "backup_efs"
  encrypted      = true
}


resource "aws_efs_mount_target" "backup_efs_mount_target" {
  file_system_id  = aws_efs_file_system.backup_efs.id
  subnet_id       = aws_subnet.private_subnet[0].id
  security_groups = [aws_security_group.sg_efs.id]
}